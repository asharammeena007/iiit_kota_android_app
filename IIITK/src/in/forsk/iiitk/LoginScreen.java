package in.forsk.iiitk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginScreen extends Activity {
	private final static String TAG = LoginScreen.class.getSimpleName();
	Context context;

	EditText nameEt, emailEt, passEt;
	Button loginBtn;

	// Dr Sylvester Fernandes - 27th August 2015 12:39 PM
	// Lets start to learn Collaborative Programming using GIT and BITBUCKET
	
	//ShyamSunder Verma - 27th August 2015 4:39 PM
	//First Change
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_screen);

		context = this;

		nameEt = (EditText) findViewById(R.id.nameEt);
		emailEt = (EditText) findViewById(R.id.emailEt);
		passEt = (EditText) findViewById(R.id.passEt);

		SharedPreferences pref = context.getSharedPreferences("shared_pref_exmple", 0);

		String mNameFromSharedPref = pref.getString("name", "");
		if (TextUtils.isEmpty(mNameFromSharedPref)) {

			Toast.makeText(context, "No data in SharedPreferences", Toast.LENGTH_SHORT).show();

		} else {

			Toast.makeText(context, "Information form SharedPreferences", Toast.LENGTH_SHORT).show();

			nameEt.setText(pref.getString("name", ""));
			emailEt.setText(pref.getString("email", ""));
			passEt.setText(pref.getString("pass", ""));
		}

		loginBtn = (Button) findViewById(R.id.loginBtn);

		loginBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String name = nameEt.getText().toString();
				String email = emailEt.getText().toString();
				String pass = passEt.getText().toString();

				if (TextUtils.isEmpty(name)) {
					nameEt.setError("Required Field");
					return;
				}
				if (TextUtils.isEmpty(email)) {
					emailEt.setError("Required Field");
					return;
				}
				if (TextUtils.isEmpty(pass)) {
					passEt.setError("Required Field");
					return;
				}

				SharedPreferences pref = context.getSharedPreferences("shared_pref_exmple", 0);
				Editor editor = pref.edit();
				editor.putString("name", name);
				editor.putString("email", email);
				editor.putString("pass", pass);
				editor.commit();

				Toast.makeText(context, "Information Saved", Toast.LENGTH_SHORT).show();

				finish();

			}
		});
	}
}
