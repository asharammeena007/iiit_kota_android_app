package in.forsk.iiitk;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;


public class _2013Kucp1017_PictureGallery_Adapter extends RecyclerView.Adapter<_2013Kucp1017_PictureGallery_Adapter.ViewHolder> {
	

    private Context context;
    private ItemClickListener itemClickListener;
    private List<_2013KUCP1017_PictureGallery_ImageWrapper> list;

    _2013Kucp1017_PictureGallery_Adapter(Context c , List<_2013KUCP1017_PictureGallery_ImageWrapper> l){
        context = c;
        list = l;
    }

    public void setItemClickListener(ItemClickListener icl){
        itemClickListener = icl;
    }

  //  <!-- 2013kucp1026 Shantanu Ramteke  PictureGallery 04/11/2015 6:26PM -->
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View rootView = LayoutInflater.from(context).inflate(R.layout._2013kucp1017_picture_gallery_album_thumbnail , viewGroup , false);

        ViewHolder holder = new ViewHolder(rootView);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        viewHolder.thumbnailName.setText(list.get(i).getGroupName());

        Picasso.with(context).load(list.get(i).groupImageLink).into(viewHolder.thumbnailImage);
        //viewHolder.thumbnailImage.setImageResource(list.get(i).getImageLink().get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView thumbnailName;
        ImageView thumbnailImage;
        RelativeLayout thumbnailHolder;

        public ViewHolder(View itemView) {
            super(itemView);

            thumbnailName = (TextView) itemView.findViewById(R.id.thumbnailName);
            thumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnailImage);
            thumbnailHolder = (RelativeLayout) itemView.findViewById(R.id.thumbnailHolder);

            thumbnailHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null){
                        itemClickListener.onClickListener(view , getAdapterPosition());
                    }
                }
            });

        }
    }
}


interface ItemClickListener{
    void onClickListener(View v , int position);
}
