package in.forsk.iiitk;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
public class _2013KUCP1017_PictureGallery_ConnectionAdapter {

	 // <!-- 2013kucp1017 Asha Ram Meena  PictureGallery 04/11/2015 6:05PM -->
    private Context _context;

    public _2013KUCP1017_PictureGallery_ConnectionAdapter(Context context){
        this._context = context;
    }
    
    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null){
                if(info.isConnected() && (info.getType() == connectivity.TYPE_ETHERNET || info.getType() == connectivity.TYPE_MOBILE || info.getType() == connectivity.TYPE_WIFI)){
                    return true;
                }
            }

        }
        return false;
    }
}
